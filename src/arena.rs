use bevy::prelude::*;
use bevy_prototype_lyon::prelude::*;
use bevy_rapier2d::prelude::*;

pub struct ArenaPlugin;

impl Plugin for ArenaPlugin {
    fn build(&self, app_builder: &mut App) {
        app_builder.add_startup_system(setup_walls);
    }
}

fn setup_walls(mut commands: Commands) {
    let ground_size = 25.0;

    let collider = ColliderBundle {
        shape: ColliderShape::cuboid(ground_size, 1.0).into(),
        ..Default::default()
    };
    commands
        .spawn_bundle(collider)
        // .insert(ColliderDebugRender::default())
        .insert(ColliderPositionSync::Discrete);

    let collider = ColliderBundle {
        shape: ColliderShape::cuboid(ground_size * 2.0, 1.2).into(),
        position: Isometry::new(
            [ground_size, ground_size * 2.0].into(),
            std::f32::consts::FRAC_PI_2,
        )
        .into(),
        ..Default::default()
    };
    commands
        .spawn_bundle(collider)
        // .insert(ColliderDebugRender::default())
        .insert(ColliderPositionSync::Discrete);

    let collider = ColliderBundle {
        shape: ColliderShape::cuboid(ground_size * 2.0, 1.2).into(),
        position: Isometry::new(
            [-ground_size, ground_size * 2.0].into(),
            std::f32::consts::FRAC_PI_2,
        )
        .into(),
        ..Default::default()
    };
    commands
        .spawn_bundle(collider)
        // .insert(ColliderDebugRender::default())
        .insert(ColliderPositionSync::Discrete)
        .insert_bundle(GeometryBuilder::build_as(
            &shapes::Rectangle {
                extents: Vec2::new(ground_size * 20., 1.2),
                origin: RectangleOrigin::default(),
            },
            DrawMode::Outlined {
                fill_mode: FillMode::color(Color::CYAN),
                outline_mode: StrokeMode::new(Color::BLACK, 0.5),
            },
            Transform::default(),
        ));
}
