extern crate rapier2d; // For the debug UI.

use bevy::prelude::*;
use bevy::sprite::MaterialMesh2dBundle;
use bevy_prototype_lyon::prelude::*;
use bevy_rapier2d::prelude::*;

pub struct BallsPlugin;

impl Plugin for BallsPlugin {
    fn build(&self, _app_builder: &mut App) {
        _app_builder
            .insert_resource(ClearColor(Color::rgb(
                0xF9 as f32 / 255.0,
                0xF9 as f32 / 255.0,
                0xFF as f32 / 255.0,
            )))
            .insert_resource(Msaa::default())
            .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
            .add_plugin(RapierRenderPlugin)
            // .add_plugin(DebugUiPlugin)
            .add_startup_system(setup_graphics.system())
            .add_startup_system(setup_physics.system())
            .add_startup_system(enable_physics_profiling.system());
    }
}

fn enable_physics_profiling(mut pipeline: ResMut<PhysicsPipeline>) {
    pipeline.counters.enable()
}

fn setup_graphics(mut commands: Commands, mut configuration: ResMut<RapierConfiguration>) {
    configuration.scale = 1.0;
}

pub fn setup_physics(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    let num = 2;
    let rad = 2.5;

    let shift = rad * 2.0;
    let centerx = shift * (num as f32) / 2.0;
    let centery = shift / 2.0;
    let mut color = 0;

    for i in 0..num {
        for j in 0usize..num * 5 {
            let x = i as f32 * shift - centerx;
            let y = j as f32 * shift + centery + 2.0;
            color += 1;

            // Build the rigid body.
            let body = RigidBodyBundle {
                position: [x, y].into(),
                ..Default::default()
            };
            let collider = ColliderBundle {
                shape: ColliderShape::ball(rad).into(),
                ..Default::default()
            };
            commands
                .spawn_bundle(body)
                .insert_bundle(collider)
                .insert_bundle(GeometryBuilder::build_as(
                    &shapes::Circle {
                        radius: rad,
                        ..shapes::Circle::default()
                    },
                    DrawMode::Outlined {
                        fill_mode: FillMode::color(Color::CYAN),
                        outline_mode: StrokeMode::new(Color::BLACK, 0.5),
                    },
                    bevy::prelude::Transform::default(),
                ))
                .insert(ColliderPositionSync::default());
        }
    }
}
