use bevy::prelude::*;

pub struct CameraPlugin;

fn init_camera(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
}

impl Plugin for CameraPlugin {
    fn build(&self, app_builder: &mut App) {
        app_builder.add_startup_system(init_camera);
    }
}
