extern crate bevy;
extern crate bevy_rapier2d;
extern crate rand;

mod arena;
mod balls;
mod camera;
mod controls;
mod map;
mod window;

use bevy::prelude::*;
use bevy_prototype_lyon::prelude::*;

use arena::ArenaPlugin;
use balls::BallsPlugin;
use bevy_ecs_tilemap::prelude::*;
use camera::CameraPlugin;
use controls::ControlsPlugin;
use window::WindowPlugin;

fn swap_that_tile(_tiles: Query<&mut Tile>, _map_query: MapQuery, _commands: Commands) {
    // map_query.get_tile_entity(TilePos { x:0, y:0 });
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(ShapePlugin)
        .add_plugin(WindowPlugin)
        .add_plugin(CameraPlugin)
        // .add_plugin(MapPlugin)
        .add_plugin(ControlsPlugin)
        .add_plugin(ArenaPlugin)
        .add_plugin(BallsPlugin)
        .add_system(swap_that_tile)
        .run();
}
