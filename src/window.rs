use bevy::prelude::*;

pub struct WindowPlugin;

impl Plugin for WindowPlugin {
    fn build(&self, app_builder: &mut App) {
        app_builder.insert_resource(WindowDescriptor {
            width: 1920.0,
            height: 1080.0,
            title: String::from("Map Example"),
            ..Default::default()
        });
    }
}
